package org.pav.app.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.pav.app.domain.Links;
import org.pav.app.domain.Students;
import org.pav.app.domain.Teachers;
import org.pav.app.repository.LinksRepository;
import org.pav.app.repository.StudentsRepository;
import org.pav.app.repository.TeachersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
public class TeachersController {

    @Autowired
    private TeachersRepository teachersRepo;
    @Autowired
    private LinksRepository linksRepo;
    @Autowired
    private StudentsRepository studentsRepo;

    @Operation(summary = "Добавление преподавателя")
    @RequestMapping(value = "/teacher/add",
            method = {RequestMethod.GET,RequestMethod.POST})
    public String addTeacherRequest(
            @RequestParam(required = false) String full_name,
            @RequestParam(required = false) String chair,
            Model model, HttpServletResponse response) {

        Teachers teach = new Teachers(full_name,chair);
        teachersRepo.save(teach);
        log.info("Добавлен преподователь "+teach.toString());
        response.setStatus(200);
        model.addAttribute("message","Добавлен преподователь "+teach.toString());
        return "msg";
    }

    @Operation(summary = "Обновление записи преподавателя")
    @RequestMapping(value = "/teacher/update",
            method = {RequestMethod.GET,RequestMethod.PUT})
    public String updateTeacherRequest(
            @RequestParam Integer id,
            @RequestParam(required = false) String full_name,
            @RequestParam(required = false) String chair,
            Model model, HttpServletResponse response) {

        if(teachersRepo.existsById(id)){
            Teachers teacher = teachersRepo.findById(id).get();
            if(full_name!=null)teacher.setFull_name(full_name);
            if(chair!=null)teacher.setChair(chair);
            response.setStatus(200);
            model.addAttribute("message","Обновлен преподаватель "+teacher.toString());
            return "msg";
        }
        response.setStatus(404);
        return "nf";
    }

    @Operation(summary = "Удаленине преподавателя по id")
    @RequestMapping(value = "/teacher/del",
            method = {RequestMethod.GET,RequestMethod.DELETE})
    public String addTeacherRequest(
            @RequestParam Integer id,
            Model model, HttpServletResponse response) {
        if(teachersRepo.existsById(id)){
            teachersRepo.deleteById(id);
            response.setStatus(200);
            return "ok";
        }
        response.setStatus(404);
        return "nf";
    }

    @Operation(summary = "Получение одного преподавателя по id")
    @RequestMapping(value = {"/teacher"},
            method = {RequestMethod.GET,RequestMethod.POST},
            produces={MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Object getOneTeacherRequest(
            @RequestParam(required = false) Integer id, HttpServletResponse response) {
        if(teachersRepo.existsById(id)){
            return teachersRepo.findById(id);
        }
        return null;
    }

    @Operation(summary = "Получение списка всех преподавателей")
    @RequestMapping(value = {"/teachers"},
            method = {RequestMethod.GET,RequestMethod.POST},
            produces={MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Iterable<Teachers> getAllTeachersRequest(
            @RequestParam(required = false) Integer id, HttpServletResponse response) {
        return teachersRepo.findAll();
    }

    @Operation(summary = "Получение списка всех студентов, которых обучает преподаватель")
    @RequestMapping(value = "/teacher/links",
            method = {RequestMethod.GET,RequestMethod.POST},
            produces={MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Iterable<Students> getTeacherLinks(
            @RequestParam Integer id,
            Model model, HttpServletResponse response) {

        List<Students> list = new ArrayList<>();
        if(teachersRepo.existsById(id)){
            Iterable<Links> links = linksRepo.findAll();

            for (Links l : links ) {
                int tid = l.getTeacher_id();
                int sid = l.getStudent_id();
                if(tid==id){
                    if(studentsRepo.existsById(sid)){
                        list.add(studentsRepo.findById(sid).get());
                    }
                }
            }
            // возвращаем лист с результатом
            response.setStatus(200);
        }
        // возвращаем пустой лист
        response.setStatus(404);
        return list;
    }


}
