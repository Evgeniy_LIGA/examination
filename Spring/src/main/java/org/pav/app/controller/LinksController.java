package org.pav.app.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.pav.app.domain.CompositeId;
import org.pav.app.domain.Links;
import org.pav.app.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
public class LinksController {

    @Autowired
    private StudentsRepository studentsRepo;
    @Autowired
    private TeachersRepository teachersRepo;
    @Autowired
    private LinksRepository linksRepo;

    public String link(Integer tid, Integer sid, Boolean contype, Model model, HttpServletResponse response) {

        // Проверяем что преподаватель не null
        if(tid==null){
            // Если связь не нашли
            model.addAttribute("message", "Status 404. Невозможно создать связь. Id преподавателя равен null!");
            response.setStatus(404);
            return "msg";
        }

        // Проверяем что студент не null
        if(sid==null){
            // Если связь не нашли
            model.addAttribute("message", "Status 404. Невозможно создать связь. Id студента равен null!");
            response.setStatus(404);
            return "msg";
        }

        // Проверяем наличие такого преподавателя
        if(!teachersRepo.existsById(tid)){
            // Если связь не нашли
            model.addAttribute("message", "Status 404. Невозможно создать связь. Преподаватель с id "+tid+" не найден.");
            response.setStatus(404);
            return "msg";
        }

        // Проверяем наличие такого студента
        if(!studentsRepo.existsById(sid)){
            model.addAttribute("message", "Status 404. Невозможно создать связь. Студент с id "+sid+" не найден.");
            response.setStatus(404);
            return "msg";
        }

        CompositeId cid = new CompositeId(tid,sid);
        if(contype==true){
            // Если нужно добавить связь
            // Проверяем если искомая связь не существует, тогда пробуем создать ее
            if(!linksRepo.existsById(cid)){
                linksRepo.save(new Links(tid,sid));
                model.addAttribute("message", "Status 200. Связь добавлена.");
                response.setStatus(200);
            } else {
                // а если она есть значит не нужно ничего создавать.
                model.addAttribute("message", "Status 200. Данная связь уже существует.");
                response.setStatus(200);
            }
        } else {
            // Если нужно удалить связь
            // Проверяем если искомая связь существует, тогда удаляем ее
            if(linksRepo.existsById(cid)){
                linksRepo.deleteById(cid);
                model.addAttribute("name", "Status 200. Связь удалена.");
                response.setStatus(200);
            } else {
                // Если связь не нашли
                model.addAttribute("name", "Status 404. Искомая связь "+
                        " tid="+tid+" sid="+sid+" не найдена.");
                response.setStatus(404);
            }
        }
        return "msg";
    }

    @Operation(summary = "Сервис по созданию/удалению связей между учителями и студентами")
    @RequestMapping(value = "link/{link_type}",
            method = {RequestMethod.GET,RequestMethod.POST})
    public String addPeopleRequest(
            @PathVariable("link_type") String link_type,
            @RequestParam(required = false) Integer tid,
            @RequestParam(required = false) Integer sid,
            Model model, HttpServletResponse response) {
        if(link_type.equals("add")) {
            return link(tid, sid, true, model, response);
        } else if (link_type.equals("del")) {
            return link(tid, sid, false, model, response);
        }
        return "error";
    }

}
