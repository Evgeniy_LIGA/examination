package org.pav.app.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.pav.app.domain.Links;
import org.pav.app.domain.Students;
import org.pav.app.domain.Teachers;
import org.pav.app.logger.GlobalLogger;
import org.pav.app.repository.LinksRepository;
import org.pav.app.repository.StudentsRepository;
import org.pav.app.repository.TeachersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentsController {

    @Autowired
    private TeachersRepository teachersRepo;
    @Autowired
    private LinksRepository linksRepo;
    @Autowired
    private StudentsRepository studentsRepo;

    @Operation(summary = "Добавление студента")
    @RequestMapping(value = "/student/add",
            method = {RequestMethod.GET,RequestMethod.POST})
    public String addStudentRequest(
            @RequestParam(required = false) String full_name,
            @RequestParam(required = false) String speciality,
            @RequestParam(required = false) String course,
            Model model, HttpServletResponse response) {

        Students stud = new Students(full_name,speciality,course);
        studentsRepo.save(stud);
        String msg = "Добавлен студент "+stud.toString();
        model.addAttribute("message",msg);
        GlobalLogger.info(msg);
        response.setStatus(200);
        return "msg";
    }

    @Operation(summary = "Обновление записи студента")
    @RequestMapping(value = "/student/update",
            method = {RequestMethod.GET,RequestMethod.PUT})
    public String updateStudentRequest(
            @RequestParam Integer id,
            @RequestParam(required = false) String full_name,
            @RequestParam(required = false) String speciality,
            @RequestParam(required = false) String course,
            Model model, HttpServletResponse response) {

        if(studentsRepo.existsById(id)){
            Students stud = studentsRepo.findById(id).get();
            if(full_name!=null)stud.setFull_name(full_name);
            if(speciality!=null)stud.setSpeciality(speciality);
            if(course!=null)stud.setCourse(course);
            response.setStatus(200);
            String msg = "Обновлен студент "+stud.toString();
            model.addAttribute("message",msg);
            GlobalLogger.info(msg);
            return "msg";
        }
        response.setStatus(404);
        return "nf";
    }

    @Operation(summary = "Удаление студента по id")
    @RequestMapping(value = "/student/del",
            method = {RequestMethod.GET,RequestMethod.DELETE})
    public String delStudentRequest(
            @RequestParam Integer id,
            Model model, HttpServletResponse response) {
        if(studentsRepo.existsById(id)){
            studentsRepo.deleteById(id);
            response.setStatus(200);
            String msg = "Удален студент с id="+id;
            model.addAttribute("message",msg);
            GlobalLogger.info(msg);
            return "msg";
        }
        response.setStatus(404);
        return "nf";
    }

    @Operation(summary = "Получение одного студента по id")
    @RequestMapping(value = {"/student"},
            method = {RequestMethod.GET,RequestMethod.POST},
            produces={MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Object getOneStudentRequest(
            @RequestParam(required = false) Integer id, HttpServletResponse response) {
            if(studentsRepo.existsById(id)){
                return studentsRepo.findById(id);
            }
            return null;
    }

    @Operation(summary = "Получение списка всех студентов")
    @RequestMapping(value = {"/students"},
            method = {RequestMethod.GET,RequestMethod.POST},
            produces={MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Iterable<Students> getAllStudentsRequest(
            @RequestParam(required = false) Integer id, HttpServletResponse response) {
        return studentsRepo.findAll();
    }

    @Operation(summary = "Получение списка всех преподавателей, у которых учится студент")
    @RequestMapping(value = "/student/links",
            method = {RequestMethod.GET,RequestMethod.POST},
            produces={MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Iterable<Teachers> getStudentLinksRequest(
            @RequestParam Integer id,
            Model model, HttpServletResponse response) {

        List<Teachers> list = new ArrayList<>();
        if(studentsRepo.existsById(id)){
            Iterable<Links> links = linksRepo.findAll();

            for (Links l : links ) {
                int tid = l.getTeacher_id();
                int sid = l.getStudent_id();
                if(sid==id){
                    if(teachersRepo.existsById(tid)){
                        list.add(teachersRepo.findById(tid).get());
                    }
                }
            }
            // возвращаем лист с результатом
            response.setStatus(200);
        }
        // возвращаем пустой лист
        response.setStatus(404);
        return list;
    }
}


