package org.pav.app.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Table(name = "students")
@XmlRootElement(name = "Root")
@Data
public class Students {

    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="users_seq")
    //@SequenceGenerator(name="s_seq",sequenceName="students_seq", allocationSize=1)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer student_id;

    private String full_name;
    private String speciality;
    private String course;

    public Students(String full_name, String speciality, String course) {
        this.full_name = full_name;
        this.speciality = speciality;
        this.course = course;
    }

    public Students() {}
}