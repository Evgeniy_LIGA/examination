package org.pav.app.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Table(name = "teachers")
@XmlRootElement(name = "Root")
@Data
public class Teachers {

    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="teachers_seq")
    //@SequenceGenerator(name="t_seq",sequenceName="teachers_seq", allocationSize=1)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer teacher_id;

    private String full_name;
    private String chair;

    public Teachers() {
    }

    public Teachers(String name, String surname) {
        this.full_name = name;
        this.chair = surname;
    }
}
