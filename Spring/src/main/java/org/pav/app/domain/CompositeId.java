package org.pav.app.domain;

import java.io.Serializable;
import java.util.Objects;

public class CompositeId implements Serializable {

    // имена переменных здесь и в классе Links должны строго совпадать
    private Integer teacher_id;
    private Integer student_id;

    public CompositeId() {
    }

    public CompositeId(Integer tid, Integer sid) {
        this.teacher_id = tid;
        this.student_id = sid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        CompositeId cid = (CompositeId) o;
        return (teacher_id.equals(cid.teacher_id) && student_id.equals(cid.student_id));
    }

    @Override
    public int hashCode() {
        return Objects.hash(teacher_id, student_id);
    }

}
