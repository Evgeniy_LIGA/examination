package org.pav.app.grpc.logger;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.pav.app.grpc.Message;
import org.pav.app.grpc.MessageServerGrpc;

public class MessageClient {

    MessageServerGrpc.MessageServerBlockingStub stub;

    public boolean Init(String name, String host, Integer port){
        System.out.println(name + ": "+"Пробуем создать подключение к GRPC " + host + ":" + port );
        boolean completed = false;
        try {
            //Создание клиентской заглушки
            ManagedChannel channel = ManagedChannelBuilder
                    .forAddress(host, port).usePlaintext().build();
            // Здесь мы используем блокирующую / синхронную заглушку, так что вызов RPC ожидает ответа сервера
            // и либо возвращает ответ, либо вызывает исключение. Существует еще два типа других заглушек.
            stub = MessageServerGrpc.newBlockingStub(channel);
            completed = true;
        } catch (Exception e){
            System.out.println(name + ": "+"Не удалось открыть канал, возможно сервер не запущен.");
        }
        return completed;
    }

    public void Log(String name){
        // Создаем запрос
        Message.PrintMessageRequest req = Message.PrintMessageRequest.newBuilder()
                .setData(name).build();
        // получаем ответ
        Message.PrintMessageResponse resp = stub.printMessage(req);
    }

}
