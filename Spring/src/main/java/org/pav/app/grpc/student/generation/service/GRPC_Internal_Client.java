package org.pav.app.grpc.student.generation.service;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.pav.app.grpc.StudentsServerGrpc;
import org.pav.app.domain.Students;
import org.pav.app.grpc.Learning;
import org.pav.app.repository.StudentsRepository;

import java.util.List;
import java.util.Random;

public class GRPC_Internal_Client {

    // Если хотим, чтобы этот класс заполнял SQL таблицу
    // данная переменная должна быть инициализирована из вне.
    public static StudentsRepository studentsRepo;

    public static void Request(String name, String host, Integer port){

        boolean completed = false;

        while(completed == false){
            try {
                System.out.println(name + ": "+"Пробуем создать подключение к GRPC " + host + ":" + port );

                //Создание клиентской заглушки
                ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();

                // Здесь мы используем блокирующую / синхронную заглушку, так что вызов RPC ожидает ответа сервера
                // и либо возвращает ответ, либо вызывает исключение. Существует еще два типа других заглушек.
                StudentsServerGrpc.StudentsServerBlockingStub stub = StudentsServerGrpc.newBlockingStub(channel);


                // Создаем запрос
                Random rnd = new Random();
                int quantity = rnd.nextInt(5)+1;

                System.out.println(name + ": "+"Подключение есть. Создаем запрос на получение списка из "+quantity+" студентов." );

                Learning.GetStudentsRequest req = Learning.GetStudentsRequest.newBuilder()
                        .setQuantity(quantity).build();

                //Отправляем запрос на отправку почты используя заглушку stub
                //sendMail - из proto service
                Learning.GetStudentsResponse resp = stub.getStudents(req);
                if (resp.getStudentsCount() > 0) {
                    System.out.println(name + ": "+"От GRPC сервера получено " + resp.getStudentsCount() + " записей.");

                    List<Learning.Student> list = resp.getStudentsList();
                    int num=1;
                    for (Learning.Student q : list ) {
                        System.out.print("Студент №"+(num++)+": ");
                        System.out.print("" + q.getFullName());
                        System.out.print("; Специальность=" + q.getSpeciality());
                        System.out.println("; Курс=" + q.getCourse()+".");
                    }

                    // Добавим студентов из этого списка в SQL таблицу применив Spring Data JPA
                    // запись будет производится только если studentsRepo будет проинициализирован из Spring приложения.
                    if(studentsRepo!=null) {
                        for (Learning.Student s : list ) {
                            Students stud = new Students(
                                    s.getFullName(),
                                    s.getSpeciality(),
                                    s.getCourse());
                            studentsRepo.save(stud);
                        }
                        System.out.println(name + ": " + "В SQL таблицу students записано " + resp.getStudentsCount() + " новых студентов.");
                    }
                    // если запрос выполнен завершаем приложение
                    completed = true;
                }
            } catch (Exception e){
                System.out.println(name + ": "+"Получен exception. Что-то пошло не так. Новая попытка через 5 секунд");
                // Задержка 5 секунд
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
                //System.out.println(e);
            }

        }
    }
}
