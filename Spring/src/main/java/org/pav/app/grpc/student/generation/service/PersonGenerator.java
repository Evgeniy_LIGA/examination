package org.pav.app.grpc.student.generation.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.Random;
import java.util.List;

/** Класс содержит в себе утилиты для работы с DTO Person.
 *  Например генерацию случайной персоны */
public class PersonGenerator {

    public static Random rnd = new Random(777);
    public static String[] sn = new String[]{
            "Иванов","Петров","Сидоров","Лебедев",
            "Андреев","Быков","Ветров","Гусев","Демин","Ежов"};
    public static String[] mn = new String[]{
            "Иван","Петр","Евгений","Степан","Юлиан","Алексей","Дмитрий"};
    public static String[] fn = new String[]{
            "Инга","Полина","Евгения","Стелла","Юлия","Анна","Дианна"};
    public static String[] mmn = new String[]{
            "Иванович","Петрович","Евгеньевич","Степанович","Поликарпович","Васильевич"};
    public static String[] fmn = new String[]{
            "Ивановна","Петровна","Евгеньевна","Степановна","Юлиановна","Васильевна"};

    public static String generateFullName(){
        boolean sex = rnd.nextBoolean();
        String fullName ="";
        String surname = sn[rnd.nextInt(sn.length)];
        if(sex){
            surname +="а";
            String name = fn[rnd.nextInt(fn.length)];
            String middle_name = fmn[rnd.nextInt(fmn.length)];
            fullName = name+" "+middle_name+" "+surname;
        } else {
            String name = mn[rnd.nextInt(mn.length)];
            String middle_name = mmn[rnd.nextInt(mmn.length)];
            fullName = name+" "+middle_name+" "+surname;
        }
        // LocalDate born_date = LocalDate.of( 1930+rnd.nextInt(80), 1+rnd.nextInt(12), 1+rnd.nextInt(27));
        // Int age = Period.between(.born_date, LocalDate.now()).getYears();
        return fullName;
    }
}
