package org.pav.app.grpc.logger;

import io.grpc.stub.StreamObserver;
import org.pav.app.grpc.Message;
import org.pav.app.grpc.MessageServerGrpc;
import org.pav.app.utils.Utils;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

/** сервер в виде отдельного приложения */
public class MessageServer extends MessageServerGrpc.MessageServerImplBase {

  public static Integer port = 50010;
  public static String name = "GRPC Server";

  public static void main(String[] args) throws IOException, InterruptedException {
    port = Utils.tryGetIntegerArgument(args,"port",port);
    name = Utils.tryGetStringArgument(args,"name",name);

    Server server = ServerBuilder.forPort(port).addService(new MessageServer()).build();
    server.start();
    System.out.println(name + ": "+"Запущен GRPC сервер логирования. порт=" + port + ".");
    server.awaitTermination();
  }

  //"Каркас" этого запроса описан в *.proto и переопределен здесь
  //printMessage(PrintMessageRequest) returns (PrintMessageResponse);
  @Override
  public void printMessage(Message.PrintMessageRequest request, StreamObserver<Message.PrintMessageResponse> responseObserver) {
    String message = request.getData();
    System.out.println(name + ": "+message);
    //Возвращаем подтверждение об отправке
    Message.PrintMessageResponse response =
            Message.PrintMessageResponse.newBuilder()
                    .setStatus(true).build();
    responseObserver.onNext(response);
    responseObserver.onCompleted();
  }

}

