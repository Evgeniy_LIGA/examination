package org.pav.app.grpc.student.generation.service;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.pav.app.grpc.Learning;
import org.pav.app.grpc.StudentsServerGrpc;
import org.pav.app.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/** сервер в виде отдельного класса приложения */
public class GRPC_Internal_Server extends StudentsServerGrpc.StudentsServerImplBase {

    public Integer port = 50002;
    public String name = "GRPC Server";

    public GRPC_Internal_Server(){
    }

    public GRPC_Internal_Server(String[] args) throws IOException, InterruptedException {
        port = Utils.tryGetIntegerArgument(args,"port",port);
        name = Utils.tryGetStringArgument(args,"name",name);

        Server server = ServerBuilder.forPort(port).addService(this).build();
        server.start();
        System.out.println(name + ": "+"Запущен сервер генерации случайных студентов " +
                "на порту " + port + ". Сервер ожидает запросов.");
        server.awaitTermination();
    }

    Random rnd = new Random();
    //"Каркас" этого запроса описан в *.proto и переопределен здесь
    @Override
    public void getStudents(Learning.GetStudentsRequest request, StreamObserver<Learning.GetStudentsResponse> responseObserver) {
        int quantity = request.getQuantity();

        List<Learning.Student> students = new ArrayList<>();
        for (int i=0; i<quantity; i++) {
            Learning.Student q = Learning.Student.newBuilder()
                    .setFullName(PersonGenerator.generateFullName())
                    .setSpeciality("Право")
                    .setCourse(""+(rnd.nextInt(4)+1)).build();
            students.add(q);
        }

        System.out.println(name + ": "+"Сервер сгенерировал " + quantity +
                " студентов.");

        //Если ничего нет - возвращаем пустой ответ, иначе возвращаем список котировок
        Learning.GetStudentsResponse response =
                Learning.GetStudentsResponse.newBuilder()
                .addAllStudents(students).build();

        System.out.println(name + ": "+"Сервер отправил клиенту, список из " + quantity +
                " студентов.");

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

}
