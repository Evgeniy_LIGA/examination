package org.pav.app.logger;

import lombok.extern.slf4j.Slf4j;
import main.java.org.pav.app.activemq.Transmitter;
import org.pav.app.grpc.logger.MessageClient;

/** Класс глобального логирования. Отсылает логи по GRPC и activeMQ*/
@Slf4j
public class GlobalLogger {

    static MessageClient mc = new MessageClient();
    static Transmitter tm = new Transmitter();
    public static void Init(){
        mc.Init("GRPC Client","localhost",50010);
        tm.Init("localhost","QUEUE");
    }

    public static void info(String string){
        // Отправляем в GRPC
        try {
            mc.Log(string);
        } catch (Exception e){
            log.warn("Не удалось отправить GRPC сообщение.");
        }
        // Отправляем в ActiveMQ
        try {
            tm.Log(string);
        } catch (Exception e){
            log.warn("Не удалось отправить ActiveMQ сообщение.");
        }
    }

}
