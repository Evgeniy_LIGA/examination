package org.pav.app;

import lombok.extern.slf4j.Slf4j;
import org.pav.app.grpc.student.generation.service.GRPC_Internal_Client;
import org.pav.app.grpc.student.generation.service.GRPC_Internal_Server;
import org.pav.app.logger.GlobalLogger;
import org.pav.app.repository.StudentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.io.IOException;

@Slf4j
@SpringBootApplication
public class ExaminationApplication {

    @Autowired
    private StudentsRepository studentsRepo;

    public static void main(String[] args) {
        SpringApplication.run(ExaminationApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    private void SpringApplicationIsReady() {
        log.info("Spring приложение успешно запущенно!");
        // Инициализируем GRPC и ActiveMQ логгер.
        GlobalLogger.Init();
        // Запускаем три сервера на разные порты в виде потоков.
        // (можно запустить и как отдельные приложения через main(),
        // но для удобства сделано так)
        new Thread(() -> runServer(new String[]{
                "name=GRPC Server №1","port=50001"
        })).start();
        new Thread(() -> runServer(new String[]{
                "name=GRPC Server №2","port=50128"
        })).start();
        new Thread(() -> runServer(new String[]{
                "name=GRPC Server №3","port=54096"
        })).start();
        // Делаем три запроса и сохраняем их результат в SQL
        // Каждый запрос подключается к определенному GRPC серверу
        // из перечисленных выше.
        // Попытки чтения производятся пока не будет получен результат.
        GRPC_Internal_Client.Request("GRPC Client Запрос #1","localhost", 50001);
        GRPC_Internal_Client.Request("GRPC Client Запрос #2","localhost", 50128);
        GRPC_Internal_Client.Request("GRPC Client Запрос #3","localhost", 54096);
    }

    public void runServer(String[] args) {
        try {
            // Создаем встроенный в Spring GRPC сервер
            GRPC_Internal_Server gs = new GRPC_Internal_Server(args);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

}
