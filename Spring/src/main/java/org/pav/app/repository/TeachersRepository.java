package org.pav.app.repository;

import org.pav.app.domain.Teachers;
import org.springframework.data.repository.CrudRepository;

public interface TeachersRepository extends CrudRepository<Teachers, Integer> {

    //@Query("select * from jobs") //если этого мало можно написать
    //List<Jobs> findAllQ();

    //@Query(value = "select * from users where name like '%smith%'", nativeQuery = true)
    //List<Jobs> findWhereNameStartsFromSmith();

}
