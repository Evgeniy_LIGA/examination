package org.pav.app.repository;

import org.pav.app.domain.Students;
import org.springframework.data.repository.CrudRepository;

// Поле @Id в dto должно быть того же типа как и -> CrudRepository<Students, ???>
public interface StudentsRepository extends CrudRepository<Students, Integer> {

    //@Query(value = "select * from students") //если этого мало можно написать
    //List<Students> findAllQ();

    //@Query(value = "select * from users where name like '%smith%'", nativeQuery = true)
    //List<Jobs> findWhereNameStartsFromSmith();

}
