package org.pav.app.repository;

import org.pav.app.domain.CompositeId;
import org.pav.app.domain.Links;
import org.springframework.data.repository.CrudRepository;

// Поле @Id в dto должно быть того же типа как и -> CrudRepository<Students, ???>
public interface LinksRepository extends CrudRepository<Links, CompositeId> {


    //@Query("select * from jobs") //если этого мало можно написать
        //собственный запрос на языке похожем на SQL
    //List<Jobs> findAllQ();

    //@Query(value = "select * from users where name like '%smith%'", nativeQuery = true)
        //если и этого мало - можно написать запрос на чистом SQL и все это будет работать
    //List<Jobs> findWhereNameStartsFromSmith();

}
