package main.java.org.pav.app.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.pav.app.utils.Utils;

import javax.jms.*;

public class Transmitter {

    public static void main(String[] args) throws Exception {
        Transmitter t = new Transmitter();
        t.Init("localhost","QUEUE");
        while(true){
            t.Log("Test");
            Thread.sleep(5000);
        }
    }

    private String queue="";
    private String host="";

    public void Init(String host, String queue) {
        this.host = host;
        this.queue = queue;
    }

    public void Log(String message) {
        try {
            ActiveMQConnectionFactory connectionFactory =
                    new ActiveMQConnectionFactory("vm://"+host);

            // Create a Connection
            Connection connection = connectionFactory.createConnection();
            connection.start();

            // Create a Session
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create the destination (Topic or Queue)
            Destination destination = session.createQueue(queue);

            // Create a MessageProducer from the Session to the Topic or Queue
            MessageProducer  producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);

            // Create a messages and Tell the producer to send the message
            TextMessage tm = session.createTextMessage(message);
            producer.send(tm);

            session.close();
            connection.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
