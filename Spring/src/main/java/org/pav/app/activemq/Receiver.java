package org.pav.app.activemq;

import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.pav.app.utils.Utils;

import javax.jms.*;

@Slf4j
public class Receiver {

    private static String name="ActiveMQ Server";
    private static String host="localhost";
    private static String queue="QUEUE";

    public static void main(String[] args) throws Exception {
        host = Utils.tryGetStringArgument(args,"host",host);
        queue = Utils.tryGetStringArgument(args,"queue",queue);

        ActiveMQReceiver amq = new ActiveMQReceiver(host,queue);
        amq.run();

        /*while(true){
            new Thread(() -> {
                ActiveMQReceiver amq = new ActiveMQReceiver(host,queue);
                amq.run();
            }).start();
            Thread.sleep(3000);
        }*/
    }

    public static class ActiveMQReceiver implements Runnable, ExceptionListener {

        private String host;
        private String queue;

        ActiveMQReceiver(String host, String queue){
            this.host = host;
            this.queue = queue;
        }

        public void run() {
            while(true) {

                try {
                    // Create a ConnectionFactory
                    ActiveMQConnectionFactory connectionFactory
                            = new ActiveMQConnectionFactory("vm://" + host);

                    // Create a Connection
                    Connection connection = connectionFactory.createConnection();
                    connection.start();

                    connection.setExceptionListener(this);

                    // Create a Session
                    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

                    // Create the destination (Topic or Queue)
                    Destination destination = session.createQueue(queue);

                    // Create a MessageConsumer from the Session to the Topic or Queue
                    MessageConsumer consumer = session.createConsumer(destination);

                    // считываем очередь.
                    for(int i=0;i<5;i++){
                        Message message = consumer.receive(10);
                        // Если сообщения есть...
                        if (message instanceof TextMessage) {
                            TextMessage textMessage = (TextMessage) message;
                            String text = textMessage.getText();
                            log.info("Info");
                            System.out.println("Received Message: '" + text + "' from '" + queue + "'");
                        } else {
                            // Если ничего не пришло, пробуем снова.
                        }
                    }

                    consumer.close();
                    session.close();
                    connection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }

        public synchronized void onException(JMSException ex) {
            System.out.println("JMS Exception occured.  Shutting down client.");
        }
    }

}
