-- База данных mydb
create sequence if not exists students_seq start 1;
CREATE TABLE students (
                          student_id      integer not null default nextval('students_seq'),
                          full_name       varchar(80),
                          speciality      varchar(50),
                          course          varchar(50)
);
create sequence if not exists teachers_seq start 1;
CREATE TABLE teachers (
                          teacher_id      integer not null default nextval('teachers_seq'),
                          full_name       varchar(80),
                          chair           varchar(50)
);
CREATE TABLE links (
                       teacher_id      integer NOT NULL,
                       student_id      integer NOT NULL,
                       PRIMARY KEY (teacher_id,student_id)
);
-- Т.к primary key по умолчанию unique такие же
-- constraint мы должны назначить одноименным
-- полям в таблицах teachers и students
ALTER TABLE teachers
    ADD CONSTRAINT teachers_unique UNIQUE (teacher_id);
ALTER TABLE students
    ADD CONSTRAINT students_unique UNIQUE (student_id);
-- Только после этого можно установить связи связи
ALTER TABLE links
    ADD FOREIGN KEY (teacher_id)
        REFERENCES teachers(teacher_id);
ALTER TABLE links
    ADD FOREIGN KEY (student_id)
        REFERENCES students(student_id);