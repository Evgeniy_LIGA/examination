package org.pav.app;

public class Utils {

    /** Безопасная инкапсулированная реализация String to Integer*/
    public static Integer StrToInt(String str, Integer integer){
        try {
            if(integer==null)return integer;
            return Integer.parseInt(str.trim());
        } catch (NumberFormatException nfe) {
            System.out.println("NumberFormatException: " + nfe.getMessage());
        }
        return integer;
    }

    /** Получить аргумент командной строки main и записать его в String*/
    public static String tryGetStringArgument(String[] args, String name, String dest){
        try {
            if(args==null)return dest;
            for(int i = 0; i < args.length; i++) {
                String[] parts = args[i].split("=");
                if(parts[0].equalsIgnoreCase(name))return parts[1];
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return dest;
    }

    /** Получить аргумент командной строки main и записать его в Integer*/
    public static Integer tryGetIntegerArgument(String[] args, String name, Integer dest){
        try {
            if(args==null)return dest;
            String str_dest = tryGetStringArgument(args, name, null);
            if(str_dest!=null)return StrToInt(str_dest,dest);
        } catch (Exception e) {
            System.out.println(e);
        }
        return dest;
    }

}
