# Для запуска приложения и разворачивания LIQUIBASE требуется:

1. Создать пользователя:

        postgres=# CREATE USER postgres WITH PASSWORD '123456';

2. И создать базу данных mydb:

        postgres=# CREATE DATABASE mydb;

3. После запуска проекта, базу данных можно заполнить некоторыми
   значениями студентов и преподавателей запустив скрипт fill_database.http
   в IntelliJ нажав "Run all requests in file".

# Возможные проблемы при запуске проекта

1. Если пользователь postgres уже существует с другим паролем.
   Пароль можно сменить на требуемый:

        postgres=# ALTER USER postgres WITH PASSWORD '123456';

2. Если база mydb уже существует и заполнена. Её можно удалить и заново создать:

        postgres=# DROP DATABASE mydb; CREATE DATABASE mydb;

# Описание стека технологий

+ Версия 1.0 (Создана на экзамене).

    + Spring framework (BOOT+WEB+JPA) - базовая основа проекта.

    + Maven - описание зависимостей pom.xml, сборка проекта, упаковка в jar.

    + PostgreSQL - описание схемы БД (init_schema.sql).

    + Liquibase - накатка changeSet на чистую SQL базу mydb (changelog-master.xml).

    + Git Flow - проект разбит на две ветви master и develop.

    + Thymeleaf - шаблонизатор для генерации index, error, message HTML страниц.

    + Lombok - применен для быстрого создания @Entity.

    + Spring Data JPA - Проведение операций с репозиториями базы данных.

    + REST API - реализация GET, POST, PUT, DELETE. STATUS (200/404) ответ сервера.

    + Swagger/Open API - генерация REST API документации.

    + Slf4j - логирование.

    + Markdown - описание настройки проекта (README.md).

+ Версия 1.1 (Доделывание, того что не успел, сразу после экзамена).

    + GRPC - создание сервера который генерирует случайного студента и клиента к нему.

<BR>

+ Версия 2.0 (Обновлена уже после экзамена)

    + ActiveMQ.
    
    + Stream API.
    
    + JUNIT Tests. 
    
    + Docker.

+ Версия 2.1

    + ActiveMQ - логгер изменений.

+ Версия 2.2

    + GRPC Сервер вынесен в отдельный модуль, со своим pom.xml.

+ Версия 2.2

    + Spring Boot вынесен в отдельный модуль, со своим pom.xml.

+ Версия 2.3

    + ActiveMQ вынесен в отдельный модуль, со своим pom.xml.

+ Версия 2.4

    + Docker создан Dockerfile для запуска GRPC сервера из контейнера.

# Как запустить:

В начале запускаем GRPC Logger из модуля GRPC-Server:

                GRPC-Server/src/main/java/org.pav.app/MessageServer.java

Потом ActiveMQ Logger из модуля ActiveMQ:

                ActiveMQ/src/main/java/org/pav/app/Receiver.java

Потом отдельно запускаем Sprint Boot из модуля Spring:

                Spring/src/main/java/org/pav/app/ExaminationApplication.java

После запуска Spring проекта, он открывает три порта на которых висят
три GRPC сервиса, которые генерируют случайные списки студентов. Далее
сам спринг отправляет себе три GRPC запроса на разные порты, получает
сгенерированные списки студентов и записывает их в SQL базу данных.
Весь процесс отображается в консоли. GRPC серверы и клиент встроенны в одно 
приложение это наглядно и удобно проверять. А логгеры сделаны как отдельные
программы.

                GRPC Client Запрос #2: Подключение есть. Создаем запрос на получение списка из 2 студентов.
                GRPC Server №2: Сервер сгенерировал 2 студентов.
                GRPC Server №2: Сервер отправил клиенту, список из 2 студентов.
                GRPC Client Запрос #2: От GRPC сервера получено 2 записей.
                Студент №1: Анна Петровна Сидорова; Специальность=Право; Курс=3.
                Студент №2: Юлия Петровна Андреева; Специальность=Право; Курс=3.
                GRPC Client Запрос #3: Пробуем создать подключение к GRPC localhost:54096
                GRPC Client Запрос #3: Подключение есть. Создаем запрос на получение списка из 2 студентов.
                GRPC Server №3: Сервер сгенерировал 2 студентов.
                GRPC Server №3: Сервер отправил клиенту, список из 2 студентов.
                GRPC Client Запрос #3: От GRPC сервера получено 2 записей.
                Студент №1: Полина Степановна Ветрова; Специальность=Право; Курс=3.
                Студент №2: Полина Юлиановна Ежова; Специальность=Право; Курс=1.

ФИО генерируются каждый запуск разные.


Проверка REST API осуществляется http тестом!

находим файл в корневой: 

                fill_database.http

И запускаем его как Run with default environment!
! Если GRPC или ActiveMQ сервер не запущен "посыпятся" ошибки в консоль.

После запуска fill_database.http в GRPC Logger будет получен результат:

                GRPC Server: Запущен GRPC сервер логирования. порт=50010.
                GRPC Server: Добавлен студент Students(student_id=7, full_name=Юлиан Васильевич Демин, speciality=экономика, course=4)
                GRPC Server: Добавлен студент Students(student_id=8, full_name=Алексей Степанович Лебедев, speciality=менеджмент, course=3)
                GRPC Server: Добавлен студент Students(student_id=9, full_name=Коннова Софья Ивановна, speciality=бух.учет, course=1)
                GRPC Server: Обновлен студент Students(student_id=2, full_name=Коннова Виктория Генадьевна, speciality=юриспруденция, course=2)

После запуска fill_database.http в ActiveMQ будет получен результат:

                SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
                SLF4J: Defaulting to no-operation (NOP) logger implementation
                SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
                SLF4J: Failed to load class "org.slf4j.impl.StaticMDCBinder".
                SLF4J: Defaulting to no-operation MDCAdapter implementation.
                SLF4J: See http://www.slf4j.org/codes.html#no_static_mdc_binder for further details.
                Received Message: 'Добавлен студент Students(student_id=34, full_name=Юлиан Васильевич Демин, speciality=экономика, course=4)' from 'QUEUE'
                Received Message: 'Добавлен студент Students(student_id=35, full_name=Алексей Степанович Лебедев, speciality=менеджмент, course=3)' from 'QUEUE'
                Received Message: 'Добавлен студент Students(student_id=36, full_name=Коннова Софья Ивановна, speciality=бух.учет, course=1)' from 'QUEUE'
                Received Message: 'Обновлен студент Students(student_id=2, full_name=Коннова Виктория Генадьевна, speciality=юриспруденция, course=2)' from 'QUEUE'

# Docker

1. Открываем server.Dockerfile из корневой и нажимаем Run server.Dockerfile

2. Настраиваем порты в контейнере localhost 50010 tcp 50010

2. Запускаем Spring/src/main/java/org/pav/app/ExaminationApplication.java

3. Запускаем fill_database.http Run with default environment

4. Проверяем полученные GRPC сообщения в Services -> контейнер -> Log 