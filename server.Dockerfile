FROM openjdk:15
LABEL maintainer="Autor"
COPY ./GRPC-Server/target/GRPC-Server-0.0.1-SNAPSHOT-jar-with-dependencies.jar /tmp/run.jar
WORKDIR /tmp
ENTRYPOINT ["java","-classpath","run.jar","org.pav.app/MessageServer"]
