# Данный скрипт создает снимок
# Базовый снимок, на котром будет строиться уже наш снимок.
# FORM должен быть один на весь докер файл.
FROM openjdk:15
# Прописываем всякую ерунду в конфигурацию
LABEL version="1.0"
LABEL description="This text illustrates \
that label-values can span multiple lines."
# Копируем из Intellij в tmp папку снимка наш "универсальный" jar
COPY ./target/MyFirstGRPC-1.0-SNAPSHOT-jar-with-dependencies.jar /tmp/run.jar
# Копируем из Intellij в tmp папку снимка наш bash скрипт чтобы запустить срузу два jar
COPY ./start.sh /tmp/
# Устанавливаем рабочую папку. (Что-то вроде cd path).
WORKDIR /tmp
# CMD или ENTRYPOINT должен быть один на весь докер файл.
# Т.к. Докер снимок имеет только "одну точку входа".
CMD ["bash","start.sh"]